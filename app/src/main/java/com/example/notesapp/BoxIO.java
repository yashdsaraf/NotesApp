package com.example.notesapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxDeveloperEditionAPIConnection;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.BoxUser;
import com.box.sdk.CreateUserParams;
import com.box.sdk.EncryptionAlgorithm;
import com.box.sdk.IAccessTokenCache;
import com.box.sdk.InMemoryLRUAccessTokenCache;
import com.box.sdk.JWTEncryptionPreferences;
import com.box.sdk.Metadata;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static android.os.Environment.*;

/**
 * Created by rorschack on 12/11/16.
 */

final class BoxIO {

    private static final String LOG_TAG = "BoxIO";
    private static final String CLIENT_ID = "";
    private static final String CLIENT_SECRET = "";
    private static final String ENTERPRISE_ID = "";
    private static final String PUBLIC_KEY_ID = "";
    private static final String PRIVATE_KEY_PASSWORD = "";
    private static final String USERS_FOLDER = "";
    private static final String NOTES_FOLDER = "";
    private static final String TT_FOLDER = "";
    private static final int MAX_CACHE_ENTRIES = 100;
    private static String userName, password;
    private String PRIVATE_KEY;
    private JWTEncryptionPreferences encryptionPref = new JWTEncryptionPreferences();
    private static BoxIO boxIO;
    private static BoxDeveloperEditionAPIConnection mApi;
    private BoxFolder users, notes, timeTable;
    private File cacheDir,filesDir, storageDir;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private Context context;


    private BoxIO(final Context ctx) throws TimeoutException {
        context = ctx;
        cacheDir = ctx.getCacheDir();
        filesDir = ctx.getFilesDir();
        if(MEDIA_MOUNTED.equals(getExternalStorageState())) {
            storageDir = getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS);
        } else {
            storageDir = Environment.getDownloadCacheDirectory();
        }
        Future<?> future = executorService.submit(new Runnable() {
            @Override
            public void run() {
                encryptionPref.setPublicKeyID(PUBLIC_KEY_ID);
                InputStream fis = ctx.getResources().openRawResource(R.raw.private_key);
                byte[] buff = null;
                try {
                    buff = new byte[fis.available()];
                    fis.read(buff);
                } catch (IOException e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
                PRIVATE_KEY = new String(buff);
                encryptionPref.setPrivateKey(PRIVATE_KEY);
                encryptionPref.setPrivateKeyPassword(PRIVATE_KEY_PASSWORD);
                encryptionPref.setEncryptionAlgorithm(EncryptionAlgorithm.RSA_SHA_256);
                IAccessTokenCache accessTokenCache = new InMemoryLRUAccessTokenCache(MAX_CACHE_ENTRIES);
                try {
                    mApi = BoxDeveloperEditionAPIConnection.getAppEnterpriseConnection(ENTERPRISE_ID, CLIENT_ID, CLIENT_SECRET,
                            encryptionPref, accessTokenCache);
                    users = new BoxFolder(mApi, USERS_FOLDER);
                    notes = new BoxFolder(mApi, NOTES_FOLDER);
                    timeTable = new BoxFolder(mApi, TT_FOLDER);

//                    mApi = BoxDeveloperEditionAPIConnection.getAppUserConnection(USER_ID, CLIENT_ID, CLIENT_SECRET,
//                            encryptionPref, accessTokenCache);
                } catch (BoxAPIException e) {
//                    if(e.getResponseCode() == 400) {
//                        Log.e(LOG_TAG,"Api is null");
//                        getNewInstance(ctx);
//                    } else {
                    Log.e(LOG_TAG,e.getResponse());
                    mApi = BoxDeveloperEditionAPIConnection.getAppEnterpriseConnection(ENTERPRISE_ID, CLIENT_ID, CLIENT_SECRET,
                            encryptionPref, accessTokenCache);
//                    }
                }
//                 mCurrentUser = BoxUser.getCurrentUser(mApi).getInfo();
            }
        });
        try {
            future.get(15, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        BoxIO.userName = userName;
    }

    public static void setPassword(String password) {
        BoxIO.password = password;
    }

    public static BoxIO getInstance(Context ctx) {
        if(mApi == null) {
            try {
                getNewInstance(ctx);
            } catch (TimeoutException e) {
                Log.e(LOG_TAG," " + e.getMessage());
            }
        } else {
            boxIO.context = ctx;
        }
        return boxIO;
    }

    public void resetFolders(Context ctx) throws TimeoutException {
        if(boxIO == null) {
            getNewInstance(ctx);
        }
        Future<Boolean> future = executorService.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                users = new BoxFolder(mApi, USERS_FOLDER);
                notes = new BoxFolder(mApi, NOTES_FOLDER);
                timeTable = new BoxFolder(mApi, TT_FOLDER);
                //noinspection ConstantConditions
                return users != null && notes != null || timeTable != null;
            }
        });
        try {
            future.get(5,TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException e) {
            Log.e(LOG_TAG,e.getMessage());
        }

    }

    public static void getNewInstance(Context ctx) throws TimeoutException {
        boxIO = new BoxIO(ctx);
    }

    public boolean createUser(String uname, String passwd) {
        try {
            return new createUserTask().execute(uname, passwd).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(LOG_TAG,e.getMessage());
        }
        return false;
    }

    private class createUserTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            String fileName = Base64.encodeToString(strings[0].getBytes(),Base64.URL_SAFE);
            File tempFile = new File(cacheDir.toString(), fileName);
            BoxFile.Info info = null;
            try {
                if(!tempFile.exists()) {
                    if(!tempFile.createNewFile()) {
                        throw new IOException("Could not create file: " + fileName);
                    }
                }
                FileInputStream fileInputStream = new FileInputStream(tempFile);
                info = users.uploadFile(fileInputStream, fileName);
                fileInputStream.close();
//                noinspection ResultOfMethodCallIgnored
                tempFile.delete();
                new BoxFile(mApi,info.getID()).createMetadata(new Metadata().add("/passwd",
                        Base64.encodeToString(strings[1].getBytes(),Base64.DEFAULT)));
                return true;
            } catch (IOException e) {
                Log.e(LOG_TAG, e.getMessage());
            } finally {
                if(tempFile.exists()) {
                    //noinspection ResultOfMethodCallIgnored
                    tempFile.delete();
                }
            }
            return false;
        }

    }

    public boolean checkUser(String uname, String passwd) {
        try {
            if(passwd == null) {
                return new checkUserTask().execute(uname).get();
            } else {
                return new checkUserTask().execute(uname, passwd).get();
            }
        } catch (InterruptedException | ExecutionException e) {
            Log.e(LOG_TAG,e.getMessage());
        }
        return false;
    }

    private class checkUserTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            for (BoxItem.Info info:users) {
                if(info.getName().equals(Base64.encodeToString(params[0].getBytes(),Base64.URL_SAFE).trim())) {
                    if(params.length <= 1) {
                        return true;
                    } else {
                        if(params[1].equals(
                                new String(Base64.decode(new BoxFile(mApi, info.getID()).getMetadata().get("/passwd"),Base64.DEFAULT)))) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

    }

    public ArrayList<String> getTT(String day) {
        try {
            return new getTTTask().execute(day).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(LOG_TAG,e.getMessage());
        }
        return null;
    }

    private class getTTTask extends AsyncTask<String, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(String... strings) {
            String filename = strings[0].toLowerCase() + ".json";
            File local = new File(filesDir.toString() + filename);
            BoxFile file = null;
            if(timeTable == null) return null;
            for (BoxItem.Info info:timeTable) {
                if(info.getName().equals(filename.trim())) {
                     file = new BoxFile(mApi,info.getID());
                    break;
                }
            }
            if(file != null) {
                FileOutputStream stream = null;
                try {
                    if(local.exists()) {
                        if(!local.delete()) {
                            throw new IOException("Could not delete " + file.toString());
                        }
                    }
                    stream = new FileOutputStream(local);
                    file.download(stream);
                    stream.close();
                    if(local.exists()) {
                        FileReader reader = new FileReader(local);
                        ArrayList<String> json = new Gson().fromJson(reader, new TypeToken<ArrayList<String>>(){}.getType());
                        reader.close();
                        return json;
                    }
                } catch (IOException e) {
                    Log.e(LOG_TAG,e.getMessage());
                }
            }
            return null;
        }
    }

    public boolean updateTT(ArrayList<String> subList, String day) {
        subList.add(0,day.toLowerCase());
        String[] subArray = subList.toArray(new String[0]);
        try {
            return new updateTTTask().execute(subArray).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(LOG_TAG,e.getMessage());
        }
        return false;
    }

    private class updateTTTask extends AsyncTask<String,Void,Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            if(timeTable == null) return false;
            for (BoxItem.Info info:timeTable) {
                if(info.getName().equals(strings[0].trim() + ".json")) {
                    new BoxFile(mApi, info.getID()).delete();
                    break;
                }
            }
            LinkedList<String> list = new LinkedList<>(Arrays.asList(strings));
            list.remove(0);
            String json = new Gson().toJson(list);
            File file = new File(filesDir.toString(),strings[0] + ".json");
            FileWriter writer;
            try {
                if(file.exists()) {
                    if(!file.delete()) {
                        throw new IOException("Could not delete " + file.toString());
                    }
                }
                writer = new FileWriter(file);
                writer.write(json);
                writer.close();
            } catch (IOException e) {
                Log.e(LOG_TAG,e.getMessage());
            }
            if(file.exists()) {
                try {
                    FileInputStream stream = new FileInputStream(file);
                    timeTable.uploadFile(stream,file.getName());
                    if(!file.delete()) {
                        throw new IOException("Could not delete " + file.toString());
                    }
                    return true;
                } catch (IOException | BoxAPIException e ) {
                    Log.e(LOG_TAG,e.getMessage());
                }
            }
            return false;
        }

    }

    public Map<String,String> getNotesList() {
        try {
            return new getNotesTask().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(LOG_TAG,e.getMessage());
        }
        return null;
    }

    private class getNotesTask extends AsyncTask<Void,Void,Map<String,String>> {

        @Override
        protected Map<String, String> doInBackground(Void... voids) {
            if(notes == null) return null;
            Map<String,String> map = new LinkedHashMap<>();
            for(BoxItem.Info info:notes) {
                map.put(info.getName(),Long.toString(info.getSize() / 1024) + "Mb");
            }
            return map;
        }
    }

    public boolean uploadNotes(Uri uri) {
        try {
            return new uploadNotesTask().execute(uri).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(LOG_TAG,e.getMessage());
        }
        return false;
    }

    private class uploadNotesTask extends AsyncTask<Uri,Void,Boolean> {

        @Override
        protected Boolean doInBackground(Uri... uris) {
            if(notes == null) return false;
            try {
                File file = new File(uris[0].getPath());
                FileInputStream stream = new FileInputStream(file);
                BoxFile.Info info = notes.uploadFile(stream,file.getName());
                stream.close();
                if(info != null) {
                    return true;
                }
            } catch (IOException e) {
                Log.e(LOG_TAG,e.getMessage());
            }
            return false;
        }

    }

    public String downloadNotes(String name) {
        try {
            return new downloadNotesTask().execute(name).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(LOG_TAG,e.getMessage());
        }
        return null;
    }

    private class downloadNotesTask extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... strings) {
            if(notes == null) return null;
            String fileName = strings[0].trim();
//            String downloadDir[] = { "Download", "Downloads", "download", "downloads"};
            String fileID = null;
            File downDir = null;
//            for(String dir:downloadDir) {
//                downDir = new File(storageDir + "/" + dir);
//                if(downDir.exists()) {
//                    break;
//                }
//            }
//            if(downDir == null) {
//                downDir = new File(storageDir + "/" + downloadDir[0]);
//                if(!downDir.mkdirs()) {
//                    Log.e(LOG_TAG,"Error writing file " + downDir.toString());
//                }
//            }
            File file = new File(filesDir,fileName);
            FileOutputStream stream = null;
            for(BoxItem.Info info:notes) {
                if(info.getName().equals(fileName)) {
                    fileID = info.getID();
                }
            }
            if(!file.canWrite()) Log.e(LOG_TAG,"Nope");
            if(fileID == null) return null;
            Log.i(LOG_TAG,"I'm here");
            BoxFile boxFile = new BoxFile(mApi,fileID);
            try {
                if(file.exists()) {
                    if(!file.delete()) {
                        throw new IOException("Could not delete " + file.toString());
                    }
                }
                stream = new FileOutputStream(file);
                boxFile.download(stream);
                stream.close();
                if(file.exists()) {
                    return file.getPath();
                }
            } catch (IOException e) {
                Log.e(LOG_TAG,e.getMessage());
            }

            return null;
        }

    }

}
