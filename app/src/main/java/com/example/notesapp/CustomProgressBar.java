package com.example.notesapp;

import android.app.Activity;
import android.content.Context;
import com.gc.materialdesign.widgets.ProgressDialog;

/**
 * Created by rorschack on 12/15/16.
 */

public class CustomProgressBar {

    private ProgressDialog progressBar;
    private static final CustomProgressBar customProgressBar = new CustomProgressBar();

    private CustomProgressBar() {
        //Singleton pattern
    }

    public static CustomProgressBar make(Context context) {
        return make(context, "Loading, please wait...");
    }
    public static CustomProgressBar make(Context context, String title) {
        customProgressBar.progressBar = new ProgressDialog(context, title);
        customProgressBar.progressBar.setCanceledOnTouchOutside(false);
        return customProgressBar;
    }

    public static void dismiss() {
        if(customProgressBar.progressBar.isShowing()) customProgressBar.progressBar.dismiss();
    }

    public static void dismiss(Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(customProgressBar.progressBar.isShowing()) customProgressBar.progressBar.dismiss();
            }
        });
    }

    public void show() {
        customProgressBar.progressBar.show();
    }
}