package com.example.notesapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

public class DialogActivity extends AppCompatActivity {

    private Intent mIntent;
    private String msg, title, negStr, posStr;
    private static DialogInterface.OnClickListener negLsnr, posLsnr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setTheme(android.R.style.Theme_Material_Light_NoActionBar);
        mIntent = getIntent();
        if(mIntent.hasExtra("pos")) posStr = mIntent.getStringExtra("pos");
        msg = mIntent.getStringExtra("msg");
        title = mIntent.getStringExtra("title");
        negStr = mIntent.getStringExtra("neg");
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setNegativeButton(negStr, negLsnr);
        if(posStr!=null) {
            alertBuilder.setPositiveButton(posStr, posLsnr);
        }
        AlertDialog alert = alertBuilder.create();
        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                finish();
            }
        });
        alert.setCanceledOnTouchOutside(false);
        alert.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        posLsnr = negLsnr = null;
    }

    public static void setListener(DialogInterface.OnClickListener n, DialogInterface.OnClickListener y) {
        negLsnr = n;
        posLsnr = y;
    }
}
