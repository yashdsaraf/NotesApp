package com.example.notesapp;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

public class LoginFragment extends Fragment {

    private EditText uname,passwd;
    private Button submit;
    private BoxIO box;
    private SharedPreferences preferences;

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getActivity().getSharedPreferences("notesapp", Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        box = BoxIO.getInstance(getContext());
        uname = (EditText)rootView.findViewById(R.id.loginUname);
        passwd = (EditText)rootView.findViewById(R.id.loginPwd);
        submit = (Button)rootView.findViewById(R.id.loginSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Snackbar snackbar = Snackbar.make(view,"",Snackbar.LENGTH_SHORT);
                hideSoftKeyboard(getActivity());
                final String username, password;
                username = uname.getText().toString().toLowerCase();
                password = passwd.getText().toString();
                Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(username);
                if(!NetworkState.isNetworkAvailable(getContext().getApplicationContext())) {
                    snackbar.setText("No internet connection").show();
                } else if(username.isEmpty()) {
                    snackbar.setText("Username cannot be empty").show();
                } else if(!matcher.find()) {
                    snackbar.setText("Username must be an e-mail ID").show();
                } else if(password.isEmpty()) {
                    snackbar.setText("Password cannot be empty").show();
                } else {
                    CustomProgressBar.make(getContext(),"Logging in...").show();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if(box.checkUser(username,password)) {
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putBoolean("isLoggedIn",true);
                                editor.putString("username",username);
                                editor.putString("password",password);
                                editor.commit();
                                makeToast("Successfully logged in!",Toast.LENGTH_SHORT);
                                if(getActivity().getCallingActivity() != null) {
                                    getActivity().setResult(RESULT_OK);
                                }
                                getActivity().finish();
                            } else {
                                Snackbar.make(view,"Invalid credentials",Snackbar.LENGTH_SHORT).show();
                            }
                            CustomProgressBar.dismiss(getActivity());
                        }
                    }).start();
                }
            }
        });
        return rootView;
    }

    void makeToast(final CharSequence csq, final int length) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(),csq,length).show();
            }
        });
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        //noinspection ConstantConditions
        View view = activity.getCurrentFocus();
        if(view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
