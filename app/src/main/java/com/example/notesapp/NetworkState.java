package com.example.notesapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeoutException;

/**
 * Created by rorschack on 12/11/16.
 */

public final class NetworkState {
    private static boolean connectionStatus;
    private static final String LOG_TAG = "NetworkState";

    public static boolean isConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting());
    }

    public static boolean isNetworkAvailable(final Context context) {
        connectionStatus = false;
        Thread T1 = new Thread(new Runnable() {
            @Override
            public void run() {
                if (isConnected(context)) {
                    try {
                        HttpURLConnection urlc = (HttpURLConnection)
                                (new URL("http://clients3.google.com/generate_204").openConnection());
                        urlc.setRequestProperty("User-Agent", "Test");
                        urlc.setRequestProperty("Connection", "close");
                        urlc.setConnectTimeout(1500);
                        urlc.connect();
                        connectionStatus = (urlc.getResponseCode() == 204 && urlc.getContentLength() == 0);
                    } catch (IOException e) {
                        Log.d(LOG_TAG, "Error checking internet connection", e);
                    }
                } else {
                    Log.d(LOG_TAG, "No network available!");
                }
            }
        });
        T1.start();
        try {
            T1.join(5000);
        } catch (InterruptedException e) {
            Log.d(LOG_TAG,e.getMessage());
        }
        return connectionStatus;
    }
}
