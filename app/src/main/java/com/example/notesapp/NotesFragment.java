package com.example.notesapp;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFloat;
import com.nononsenseapps.filepicker.FilePickerActivity;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeoutException;

public class NotesFragment extends Fragment {

    private SwipeRefreshLayout swipeRefreshLayout;
    private BoxIO box;
    private ListView listView;
    private static final int FILE_CODE = 1234;
    private Map<String,String> map;
    private AlertDialog dialog;

    public NotesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        box = BoxIO.getInstance(getContext());
        final View rootView = inflater.inflate(R.layout.fragment_notes, container, false);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainerNotes);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });

        listView = (ListView) rootView.findViewById(R.id.list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TableLayout tableLayout = (TableLayout)view;
                TableRow childAt = (TableRow) (tableLayout.getChildAt(0));
                TextView textView = (TextView) childAt.getChildAt(0);
                showDialog(textView.getText().toString());
            }
        });

        refresh();

        ButtonFloat buttonFloat = (ButtonFloat)rootView.findViewById(R.id.addnote);
        buttonFloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),FilePickerActivity.class);
                intent.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
                intent.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
                intent.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
                startActivityForResult(intent, FILE_CODE);
            }
        });

        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        );
        // Inflate the layout for this fragment
        return rootView;
    }

    private class CustomList extends ArrayAdapter<String> {

        private final String[] name;
        private final String[] createdAt;
        public CustomList(String[] name, String[] createdAt) {
            super(getActivity(), R.layout.list_singleitem, name);
            this.name = name;
            this.createdAt = createdAt;

        }
        @Override
        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View rowView= inflater.inflate(R.layout.list_singleitem, null, true);
            TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
            TextView txtCreatedAt = (TextView) rowView.findViewById(R.id.txt1);
            txtTitle.setText(name[position]);
            txtCreatedAt.setText(createdAt[position]);
            return rowView;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == FILE_CODE && resultCode == Activity.RESULT_OK) {
            if (data.getBooleanExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false)) {
                // For JellyBean and above
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    ClipData clip = data.getClipData();

                    if (clip != null) {
                        for (int i = 0; i < clip.getItemCount(); i++) {
                            Uri uri = clip.getItemAt(i).getUri();
                            if(!box.uploadNotes(uri)) {
                                Toast.makeText(getContext(),uri.toString(),Toast.LENGTH_SHORT).show();
                            };
                        }
                    }
                    // For Ice Cream Sandwich
                } else {
                    ArrayList<String> paths = data.getStringArrayListExtra
                            (FilePickerActivity.EXTRA_PATHS);

                    if (paths != null) {
                        for (String path: paths) {
                            Uri uri = Uri.parse(path);
                            if(!box.uploadNotes(uri)) {
                                Toast.makeText(getContext(),uri.toString(),Toast.LENGTH_SHORT).show();
                            };
                        }
                    }
                }

            } else {
                Uri uri = data.getData();
                if(box.uploadNotes(uri)) {
                    Toast.makeText(getContext(),uri.toString(),Toast.LENGTH_SHORT).show();
                };
            }
        }
    }

    public void refresh() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    map = box.getNotesList(); }
                catch (NullPointerException e) {
                    Log.e("Notes",e.getMessage());
                }
                String[] values = null, keys = null;
                if (map != null) {
                    values = map.values().toArray(new String[0]);
                    keys = map.keySet().toArray(new String[0]);
                    final CustomList adapter = new CustomList(keys,values);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            listView.setAdapter(adapter);
                        }
                    });
                }
            }
        }).start();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void showDialog(final String filename) {
        dialog = new AlertDialog.Builder(getContext())
                .setTitle("Would you like to download " + filename + "?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String path = null;
                        try {
                            path = box.downloadNotes(filename);
                        } catch (NullPointerException e) {
                            Log.e("Notes",e.getMessage());
                            try {
                                box.resetFolders(getContext());
                                path = box.downloadNotes(filename);
                            } catch (TimeoutException e1) {
                                Log.e("Notes",e.getMessage());
                            }
                        }
                        if(path == null) {
                            Toast.makeText(getContext().getApplicationContext(),"Error downloading " + filename,Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getContext().getApplicationContext(),"Download complete " + path,Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

}
