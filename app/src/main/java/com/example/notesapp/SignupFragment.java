package com.example.notesapp;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;



public class SignupFragment extends Fragment {


    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private EditText uname, passwd;
    private Button submit;
    private BoxIO box;
    private SharedPreferences preferences;

    public SignupFragment() {
        // Required empty public constructor
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        //noinspection ConstantConditions
        View view = activity.getCurrentFocus();
        if(view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getActivity().getSharedPreferences("notesapp", Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);
        box = BoxIO.getInstance(getContext());
        uname = (EditText) rootView.findViewById(R.id.signupUname);
        passwd = (EditText) rootView.findViewById(R.id.signupPwd);
        submit = (Button) rootView.findViewById(R.id.signupSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Snackbar snackbar = Snackbar.make(view,"",Snackbar.LENGTH_SHORT);
                hideSoftKeyboard(getActivity());
                final String username, password;
                username = uname.getText().toString().toLowerCase();
                password = passwd.getText().toString();
                Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(username);
                if(!NetworkState.isNetworkAvailable(getContext().getApplicationContext())) {
                    snackbar.setText("No internet connection").show();
                } else if (username.isEmpty()) {
                    snackbar.setText("Username cannot be empty").show();
                } else if (!matcher.find()) {
                    snackbar.setText("Username must be an e-mail ID").show();
                } else if (password.isEmpty()) {
                    snackbar.setText("Password cannot be empty").show();
                } else {
                    CustomProgressBar.make(getContext(),"Signing up...").show();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (box.checkUser(username, null)) {
                                Snackbar.make(view, "User already exists", Snackbar.LENGTH_SHORT).show();
                            } else {
                                box.createUser(username,password);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putBoolean("isLoggedIn",true);
                                editor.putString("username",username);
                                editor.putString("password",password);
                                editor.commit();
                                makeToast("Successfully signed up!", Toast.LENGTH_SHORT);
                                if (getActivity().getCallingActivity() != null) {
                                    getActivity().setResult(RESULT_OK);
                                }
                                getActivity().finish();
                            }
                            CustomProgressBar.dismiss(getActivity());
                        }
                    }).start();
                }
            }
        });
        return rootView;
    }

    void makeToast(final CharSequence csq, final int length) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(),csq,length).show();
            }
        });
    }

}
