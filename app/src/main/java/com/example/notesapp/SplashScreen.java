package com.example.notesapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.concurrent.TimeoutException;

import static com.example.notesapp.NetworkState.isNetworkAvailable;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        new SplashTask().execute();
    }


    private class SplashTask extends AsyncTask<Void, Void, Void> {

        boolean isOffline = true;

        @Override
        protected Void doInBackground(Void... voids) {
            if (isNetworkAvailable(getApplicationContext()))
                try {
                    BoxIO.getNewInstance(getApplicationContext());
                    isOffline = false;
                } catch (TimeoutException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Poor internet connection!", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            else {
                try {
                    Thread.sleep(1500);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "No Internet Connection!", Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (InterruptedException e) {
                    Log.e("Splash", e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Intent intent = new Intent(SplashScreen.this, MainActivity.class);
            intent.putExtra("isOffline", isOffline);
            startActivity(intent);
            finish();
        }
    }
}
