package com.example.notesapp;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFloat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeoutException;

import static com.example.notesapp.NetworkState.isNetworkAvailable;

public class TTFragment extends Fragment {

    private View mainView;
    private int day;
    private Spinner daySpinner;
    private Map<Integer, TextView> map;
    private int[] keys = {
            R.id.sub1,
            R.id.sub2,
            R.id.sub3,
            R.id.sub4,
            R.id.sub5,
            R.id.sub6
    };
    private SwipeRefreshLayout swipeRefreshLayout;
    private BoxIO box;
    private ArrayList<String> updatedSubs;

    public TTFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_tt, container, false);
        day = Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_WEEK);
        daySpinner = (Spinner) mainView.findViewById(R.id.fixedDay);
        daySpinner.setSelection(day - 1, true);
        map = new LinkedHashMap<>();
        box = BoxIO.getInstance(getContext());
        for (int key : keys) {
            map.put(key, (TextView) mainView.findViewById(key));
        }
        swipeRefreshLayout = (SwipeRefreshLayout) mainView.findViewById(R.id.swipeContainerTT);
        refresh();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });

        ButtonFloat buttonFloat = (ButtonFloat) mainView.findViewById(R.id.update);
//
        buttonFloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TTUpdateActivity.class));

            }
        });

        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_dark,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_purple
        );
        // Inflate the layout for this fragment
        return mainView;
    }

    public void refresh() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    updatedSubs = box.getTT((String) daySpinner.getSelectedItem());
                } catch (NullPointerException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });
                    return;
                }
                if (updatedSubs == null) {
                    if (!isNetworkAvailable(getContext())) {
                        makeToastOnUi("Please check your internet connection!");
                    } else {
                        try {
                            box.resetFolders(getContext());
                        } catch (TimeoutException e) {
                            e.printStackTrace();
                            makeToastOnUi("Please check your internet connection!");
                            return;
                        }
                        ArrayList<String> newUpdatedSubs = box.getTT((String) daySpinner.getSelectedItem());
                        if (newUpdatedSubs == null) {
                            makeToastOnUi("Time table not found");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    swipeRefreshLayout.setRefreshing(false);
                                }
                            });
                            return;
                        }
                        updatedSubs = newUpdatedSubs;
                    }
                }
                int index = 0;
                try {
                    for (Map.Entry<Integer, TextView> entry : map.entrySet()) {
                        entry.getValue().setText(updatedSubs.get(index));
                        index++;
                    }
                } catch (NullPointerException e) {
                    Log.e("TT",e.getMessage());
                }
            }
        }).start();
        swipeRefreshLayout.setRefreshing(false);
    }

    private void makeToastOnUi(final CharSequence cs) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext().getApplicationContext(),cs, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
