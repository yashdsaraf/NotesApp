package com.example.notesapp;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.gc.materialdesign.views.ButtonFloat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import static com.example.notesapp.NetworkState.isNetworkAvailable;

public class TTUpdateActivity extends AppCompatActivity {

    private ButtonFloat buttonFloat;
    private ArrayList<String> subArray;
    private Spinner spinner1,spinner2,spinner3,spinner4,spinner5,spinner6,daySpinner;
    private int day;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ttupdate);
        day = Calendar.getInstance(TimeZone.getDefault()).get(Calendar.DAY_OF_WEEK);
        subArray = new ArrayList<>();
        spinner1 = (Spinner)findViewById(R.id.spinner1);
        spinner2 = (Spinner)findViewById(R.id.spinner2);
        spinner3 = (Spinner)findViewById(R.id.spinner3);
        spinner4 = (Spinner)findViewById(R.id.spinner4);
        spinner5 = (Spinner)findViewById(R.id.spinner5);
        spinner6 = (Spinner)findViewById(R.id.spinner6);
        daySpinner = (Spinner)findViewById(R.id.daySpinner);
        daySpinner.setSelection(day - 2, true);
        //TODO: Add logic to load last known TT
        buttonFloat = (ButtonFloat)findViewById(R.id.updatable);
        buttonFloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar = Snackbar.make(view,"",Snackbar.LENGTH_SHORT);
                if(subArray.size() > 0) {
                    subArray.clear();
                }
                subArray.add((String)spinner1.getSelectedItem());
                subArray.add((String)spinner2.getSelectedItem());
                subArray.add((String)spinner3.getSelectedItem());
                subArray.add((String)spinner4.getSelectedItem());
                subArray.add((String)spinner5.getSelectedItem());
                subArray.add((String)spinner6.getSelectedItem());
                if(BoxIO.getInstance(TTUpdateActivity.this).updateTT(subArray, (String)daySpinner.getSelectedItem())) {
                    Toast.makeText(getApplicationContext(),"Updated!",Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    if(!isNetworkAvailable(TTUpdateActivity.this)) {
                        snackbar.setText("No internet connection").show();
                    } else {
                        snackbar.setText("Error! Please try again").setDuration(Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}
